

$(document).ready(function() {
	fixedMenuControls();

    $("#menu-opener").click(toggleMenuState);
    $(".js-close-menu").click(closeMenu);
    $(".js-toggle-slide").on("click", toggleSlideHandler);

    if ($('.common-home').length > 0) {
      commonHomeOnLoad();
    }

    if ($('.productPage').length > 0) {
    	productProductOnLoad();
	}

    $(".js-tabs").click(function() {

        var ACTIVE_CLASS = "is-active";

        var tabs = $($(this).data("tabs"));
        var panel = tabs.find($(this).data("panel"));

        tabs.find("."+ACTIVE_CLASS).removeClass(ACTIVE_CLASS);
        $($(this).data("panel") + "-link").addClass(ACTIVE_CLASS);
        panel.addClass(ACTIVE_CLASS);
    });

    $(".js-scroll-to").click(function() {
        var offset = $(this).data("offset") ? $(this).data("offset") : 0;
        $('html, body').animate({
            scrollTop: $($(this).data("target")).offset().top + offset
        }, 1000);
    });

});

$(window).scroll(function() {
	fixedMenuControls();
});

function toggleMenuState(){
    $("#menu").toggleClass("is-open");
    $("#menu-opener").toggleClass("is-open");
}

function closeMenu(){
    $("#menu").removeClass("is-open");
    $("#menu-opener").removeClass("is-open");
}

function fixedMenuControls(){
	if($(window).scrollTop()>100){
        $(".header").addClass("view-small");
        $(".menu-dropdownBox").addClass("view-small");
	}
	else{
        $(".header").removeClass("view-small");
        $(".menu-dropdownBox").removeClass("view-small");
	}
}

// .common-home
function commonHomeOnLoad() {
    var hpSlider = $('#hpSlider').addClass('owl-carousel').owlCarousel({
        items: 1,
        autoPlay: 3000,
        dost: false,
        loop: true,
		nav: true,
		navText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']
    });
    $("#hpSlider-prev").click(function() {
        hpSlider.trigger("prev.owl.carousel");
    });
    $("#hpSlider-next").click(function() {
        hpSlider.trigger("next.owl.carousel");
    });

    var referenceSlider = $('#referenceSlider').owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        autoplay: true,
        dots: false
    });
    $("#referenceSlider-prev").click(function() {
        referenceSlider.trigger("prev.owl.carousel");
    });
    $("#referenceSlider-next").click(function() {
        referenceSlider.trigger("next.owl.carousel");
    });
}

// .product-product .productPage
function productProductOnLoad() {
	function applyVariantConstraints () {
		var option1kg = document.querySelector('.input-option-value-1kg')
		var option250g = document.querySelector('.input-option-value-250g')
		if (option1kg && option1kg.selected) {
			document.querySelector('.input-option-value-plast input').checked = true;
			document.querySelector('.input-option-value-sklo input').disabled = true;
		} else if (option250g && option250g.selected) {
			document.querySelector('.input-option-value-sklo input').disabled = false;
		}
	}
	
	function updatePrice() {
		applyVariantConstraints()

		var currency = ' Kč' // TODO: parse the currency dynamically
		var priceEl = $('.optionBox-price')
		var basePrice = parseFloat(priceEl.attr('data-base-price'))
		var quantity = $('#input-quantity').val()
		console.log(arguments.callee.name + '(): START: priceEl', priceEl.html(), 'basePrice', basePrice, 'quantity', quantity)

		var selects = [];
		var pricePrefixes = 0.0;

		$('select[name^=option] :selected').each(function () {
			var pricePrefix = parseFloat($(this).attr('data-price-prefix'))
			console.log('--> select', $(this).attr('class'), 'pricePrefix', pricePrefix)
			if (pricePrefix > 0) {
				pricePrefixes += pricePrefix
			}
		});

		$('input[name^=option]:checked').each(function () {
			var pricePrefix = parseFloat($(this).attr('data-price-prefix'))
			console.log('--> input', $(this).attr('class'), 'pricePrefix', pricePrefix)
			if (pricePrefix > 0) {
				pricePrefixes += pricePrefix
			}
		})

		priceEl.html(((basePrice + pricePrefixes) * quantity) + ' ' + currency)
		console.log(arguments.callee.name + '(): FINISH:', 'basePrice:', basePrice, 'quantity:', quantity, 'pricePrefixes', pricePrefixes)
	}
	$('#input-quantity, select[name^=option]').on('keyup click', function () {
		console.log('#input-quantity change to:', $(this).val())
		updatePrice()
	});
	$('select[name^=option]').on('change', function () {
		console.log('select[name^=option] change to:', $(this).val())
		updatePrice()
	});
	$('input[name^=option]').on('click', function () {
		console.log('input[name^=option]', $(this).attr('class'), 'click to', $(this).val())
		updatePrice()
	})
}

function toggleSlideHandler(e){
    console.log(e);
    var opener = $(e.currentTarget);
    var block = $(opener.data("target"));
    var content = block.find(".js-toggle-slide-content");
    content.slideToggle();
    block.toggleClass("is-open");
}
