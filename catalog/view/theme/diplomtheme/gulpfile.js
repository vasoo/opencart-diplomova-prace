var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var globbing = require('gulp-css-globbing');
const autoprefixer = require('gulp-autoprefixer');
var reload = browserSync.reload;
var exec = require('child_process').exec;
var cleanCSS = require('gulp-clean-css');

gulp.task('clean', function (cb) {
    exec('rm -v vqmod/vqcache/* vqmod/*.cache', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('sass', function() {
  return gulp.src('assets/styles/index.sass')
  	.pipe(globbing({
        // Configure it to use SCSS files
        extensions: ['.sass']
    }))
  	.pipe(sass().on('error', sass.logError))
  	.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('stylesheet'))
    .pipe(reload({ stream:true }));
});

// watch Sass files for changes, run the Sass preprocessor with the 'sass' task and reload
gulp.task('watch', ['sass'], function() {
  browserSync({
    server: {
      baseDir: 'assets'
    }
  });

  gulp.watch('./assets/styles/*.sass', ['sass']);
  gulp.watch('./assets/styles/*/*.sass', ['sass']);
});

gulp.task('default', [/*'clean'*/, 'sass']);





