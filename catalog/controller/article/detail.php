<?php
class ControllerArticleDetail extends Controller {
	public function index() {
		// Optional. This calls for your language file
		$this->load->language('information/information');
		
		// Optional. Set the title of your web page
		$this->document->setTitle($this->language->get('heading_title'));

		// Breadcrumbs for the page
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Články',
			'href' => $this->url->link('article/list')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => 'Název článku',
			'href' => $this->url->link('article/detail')
		);

		// Get "heading_title" from language file
		$data['heading_title'] = 'Article list :)';
		
		// All the necessary page elements
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		// Load the template file and show output
		$this->response->setOutput($this->load->view('article/detail', $data));
	}
}