# Diplomová práce

## Vytvoření front-end aplikace pro elektronický obchod

Tento balíček je výsledkem diplomové práce vypracované na 
Provozně ekonomické fakultě České zemědělské univerzity v Praze pod vedením Ing. Josefa Pavlíčka, Ph.D.

Autorem této práce je Bc. Václav Oppelt.

## Návod pro spuštění

1. stáhnout balíček do kořenového adresáře systému OpenCart
2. V administraci přidat rozšíření s názvem Diplom Theme
3. Aktivovat příslušné téma v nastavení konkrétního obchodu

## Provádění změn
Pro úpravu souborů css, je nutné mít nainstalován následující software

* Node.js
* Yarn nebo Npm
* Gulp.js

Pro instalaci potřebných knihoven spusťte ve složce catalog\view\theme\diplomtheme 
příkaz yarn install.

Poté stačí upravit zdrojové sass soubory a spustit příkaz gulp sass, který vygeneruje
výsledné css soubory.
